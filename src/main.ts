import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";

// Vue-Touch
// @ts-ignore
import VueTouch from "vue-touch";

Vue.component("v-touch", {});
Vue.use(VueTouch);

// Font Awesome
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import {library} from "@fortawesome/fontawesome-svg-core";
import {fal} from "@fortawesome/pro-light-svg-icons";

library.add(fal);
Vue.component("font-awesome-icon", FontAwesomeIcon);

// @ts-ignore
import VueSession from "vue-session";

Vue.use(VueSession);

import "@/assets/stylesheets/master.scss";

Vue.config.productionTip = false;

import axios from "axios";

axios.defaults.baseURL = (process.env.NODE_ENV === "development" ? "http" : "https") + `://${location.hostname}:9090/`;
Vue.prototype.$axios = axios;

new Vue({
    router,
    render: h => h(App)
}).$mount("#app");

/*
De @ts-ignore is bij sommige packages toegepast omdat zij intern (via de Intellij Editor die ik gebruik) een foutmelding geven.
De packages zijn gewoon goed geïnstalleerd maar om een of andere reden blijft de fout opduiken.
Om deze foutmelding te ontwijken heb ik @ts-ignore gebruikt zodat de app gewoon werkt en Intellij niet moeilijk doet.
 */
