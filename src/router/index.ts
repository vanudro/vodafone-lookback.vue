import Vue from "vue";
import VueRouter, {RouteConfig} from "vue-router";
import Story from "../views/StoryView.vue";
import Main from "../views/MainView.vue";
import Logout from "../views/LogoutView.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
    {
        path: "/",
        alias: "/main",
        name: "Vodafone app",
        component: Main
    },
    {
        path: "/story",
        name: "Story",
        component: Story
    },
    {
        path: "/logout",
        name: "Logout",
        component: Logout
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

export default router;
