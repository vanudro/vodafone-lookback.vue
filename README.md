# The Vodafone Retrospective Moment
The Vodafone Retrospective Moment is a project made by [iDA Mediafoundry](https://www.weareida.digital/nl-en.html) for Vodafone to give Vodafone customers a retrospective moment of six months.  
The app uses customer data to give a customer insights about their subscription and what they can improve about this subscription, such as a cheaper price or a larger subscription.

### Before you start
This is only the front end of the app.  
For the full app, you also need the backend Spring Boot application which can be found [here](https://bitbucket.org/vanudro/vodafone-lookback.springboot/src/master/).
This is a Vue.js application.  If you're not familiar with Vue, please head to [their website](https://vuejs.org/) and learn more about this framework

## Requirements
- This project uses npm to install and build the application. Head to the [installation page](https://www.npmjs.com/get-npm) to install npm.
- The project depends on Font Awesome Pro. Make sure you've read their [documentation](https://fontawesome.com/v5.15/how-to-use/on-the-web/setup/using-package-managers#installing-pro) to install Font Awesome Pro.

## Installation

### Setting up Font Awesome Pro
Create a `.npmrc` file in the root directory and add the following lines to the file:
```
@fortawesome:registry=https://npm.fontawesome.com/
//npm.fontawesome.com/:_authToken={FONTAWESOME_NPM_AUTH_TOKEN}
```

### Project setup
To `install` the project after cloning, open a terminal and change your directory to the desired folder where you cloned the project and run:
```
npm install
```

#### Compiles and hot-reloads for development
To run the app locally in development mode, you'll have to `serve` the app using:
```
npm run serve
```

#### Compiles and minifies for production
To `build` the app and compile the project into basic HTML, CSS and JavaScript, run:
```
npm run build
```

#### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Contact & disclosure
This code was developed as a graduation project at the start of Februari 2021 until the end of June 2021.  
This project belongs to [iDA Mediafoundry](https://www.weareida.digital/nl-en.html). If you want to know more, please contact me at [robin.vanuden@ida-mediafoundry.nl](mailto:robin.vanuden@ida-mediafoundry.nl) or the company directly [here](https://www.weareida.digital/nl-en/contact.html).
